import pytest
import tracemalloc
from sort import inc
from sort import get_time
from test_helper import print_simple_trace
from test_helper import bubble_sort
from test_helper import unsorted_list
from test_helper import get_total_mem_used


def test_answer():
    assert inc(3) == 4


def test_is_sorted():
    t = get_time()
    x = unsorted_list(10000, -10000, 10000)
    bubble_sort(x)
    prev = -100000000000
    for element in x:
        assert element >= prev
        prev = element
    w = get_time()
    print("Total time is " + str(w - t))
    assert (w - t) < 5000

